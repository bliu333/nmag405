
## NMAG405: Universal Algebra at Charles University in Prague, Winter 2020

### Practicals

**Time:** Thursdays 10:40--12:10  
**Dates:** 8 Oct 2020 through 7 Jan 2021  
**Instructor:**  William DeMeo  
**Email:** [williamdemeo@gmail.com](mailto:williamdemeo@gmail.com)  

-------------------------------------------------------

### Table of Contents

+ [Course Web Pages](#course-web-pages)
+ [Homework Submissions](#homework-submissions)
+ [Notes](#notes)
+ [Textbook](#textbook)
+ [Grading Policy](#grading-policy)
+ [Other Resources](#other-resources)

------------------------------------------------------

### Course Web Pages

- [Libor Barto's Teaching Page](https://www2.karlin.mff.cuni.cz/~barto/students.html)

- [Practicals Web Page (this page)](https://gitlab.com/universalalgebra/nmag405)

----------------------------------------------------

## Homework Submission

### Gitlab account setup

1. Go to [gitlab.com](https://gitlab.com/) and register for a gitlab account if you don't already have one.

2. Once you have registered for and signed into an account, find your username by clicking on your profile in the top right of the browser.  In the example shown below, my username is williamdemeo.

   ![example](.misc/examplegitlab.png)
   
3. Email your gitlab username to me at [williamdemeo@gmail.com](mailto:williamdemeo@gmail.com).

Once I receive your gitlab username, I will set up a private homework repository for you and send you an invitation to gain access to that repository.  Then you will follow the instructions below to upload your homework files to your gitlab repository.

If you have any questions, you can email me at [williamdemeo@gmail.com](mailto:williamdemeo@gmail.com).

### Upload your pdf file to gitlab

In order to submit the homework, you will upload a pdf file with your solutions to the gitlab repository that was created for you in the previous step. There are two alternative ways to do this.  You only need to do one of these.

#### The Easy Way

The easiest way to upload your homework pdf file is to navigate to the appropriate directory of your gitlab repository and find the **+ v** drop-down menu and select "Upload file".  (See example in the figure below.)

   ![example](.misc/example-uploadfile.png)

#### The Hard Way

If "The Easy Way" works for you, then you can ignore the instructions in this section.

Another option for submitting your pdf homework files is to install git on your own computer, download your homework repository, and interact with the repository locally, on your own machine.  This involves several steps and is not recommended unless you are quite comfortable with computers.

1. Install git

   For this approach, you must install some version of the git program on your computer.

   In my opinion, the easiest git interface is the command line.

   + **command line git for Linux**

   You can install the command line version of git on Ubuntu Linux by entering

   ```
   sudo apt install git
   ```

   in a terminal window.

   + **command line git for Windows/MacOS**

   Please visit [the page](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html)

   [Start using git on the command line](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html)

   + **GUI git for Windows/MacOS**

   If you use a Mac or Windows OS, then you will probably want to install the [github desktop](https://desktop.github.com/** which will provide you with a GUI interface to your gitlab repository.


2. Clone (download) your homework repository

   Instructions for this step are on [this page](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-via-https), but here's a summary.

   + Visit your repository at gitlab.com. The url will be something like this:

     https://gitlab.com/universalalgebra/charlesuniversity/your-name/nmag405-homework-name
   
   + Click on the blue "Clone" button and copy the HTTP link shown in the box. (See example below.)

     ![cloneexample](.misc/clone.png)
   
   + Put this address in the appropriate field in your GUI git client and proceed

     **OR** 
   
     use the command line version of git and enter the following:
   
     ```
     git clone https://gitlab.com/universalalgebra/charlesuniversity/your-name/nmag405-homework-name.git
     ```

3. Push your pdf file to gitlab

   Instructions for this step are on [this page](https://docs.gitlab.com/ee/gitlab-basics/add-file.html), but here's a summary.

   + Add your pdf file (containing your hw solutions) to your newly cloned local copy of your nmag405-homework repository.

   + Commit your changes.

   + [Push]() your changes to Next, cpush your revised 


-------------------------------------------

### Notes

Very rough and unedited notes that we discuss during practicals are available in the [notes directory](https://gitlab.com/universalalgebra/nmag405/-/tree/master/notes) above.

-----------------------------------------


### Textbook

+ **Author**. Bergman, Clifford
+ **Title**. Universal Algebra: fundamentals and selected topics
+ **Year**. 2012
+ **Publisher**. CRC Press, Boca Raton, FL


```latex
@BOOK{Bergman:2012,
  AUTHOR = {Bergman, Clifford},
  TITLE = {Universal {A}lgebra: fundamentals and selected topics},
  YEAR = {2012},
  SERIES = {Pure and Applied Mathematics (Boca Raton)},
  VOLUME = {301},
  PUBLISHER = {CRC Press, Boca Raton, FL},
  PAGES = {xii+308},
  ISBN = {978-1-4398-5129-6},
  MRCLASS = {08-02 (06-02 08A40 08B05 08B10 08B26)},
  MRNUMBER = {2839398 (2012k:08001)},
  MRREVIEWER = {Konrad P. Pi{\'o}ro}
}
```


----------------------------------------------------

### Grading Policy

**Practicals**. homeworks (60% from 4 best scores out of 5 homeworks)

**Lecture**. 15% homeworks, 85% written test + possible oral examination

------------------------------------------------------------------


### Other Resources

+ [The Universal Algebra Calculator](http://uacalc.org/)

